#!/usr/bin/env python

""" scrape.py: Scrapes a list of Google subsidiaries as listed in Wikipedia"""

__author__ = "hardwyrd"
__copyright__ = "hardwyrd"
__license__ = "DBAD v1"

from bs4 import BeautifulSoup
import requests
import argparse

# What's the URL of the wiki page?
wiki = "https://en.wikipedia.org/wiki/List_of_mergers_and_acquisitions_by_Google"

# We just prop / spoof a useragent string so that Wikipedia won't worry too much about us.
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:39.0) Gecko/20100101 Firefox/39.0'}

# fetch the response
response = requests.get(wiki, headers=headers)

# let's parse it
soup = BeautifulSoup(response.text, "html.parser")

# we are only interested in the table
table = soup.find("table", {"class" : "wikitable sortable"})
# print table <-- forget this. I just put it there to check if I'm getting the right stuff

subsidiaries = []

# there might be better ways than this
# but this is a hack, so who cares? :)
for row in table.findAll("tr"):
    cells = row.findAll("td")
    if len(cells) == 8:
        subsidiaries.append(cells[2].find(text=True))

# we sort it before we start printing
subsidiaries.sort()

# go print!
for subsidiary in subsidiaries:
    print "{}".format(subsidiary)

